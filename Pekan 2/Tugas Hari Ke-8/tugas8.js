
// Soal No 1 

// luas lingkaran
const luasLingkaran = (r) => 3.14 * r**2


// keliling lingkaran
const kelilingLingkaran = (r) => 2 * 3.14 * r

let r = 8

console.log(`Luas Lingkaran = ${luasLingkaran(r)}`)
console.log(`Keliling Lingkaran = ${kelilingLingkaran(r)}`)
console.log('\n')



// soal No 2

const saySaya = () => 'saya'
const sayAdalah = () => 'adalah'
const saySeorang = () => 'seorang'
const sayFrontend = () => 'frontend'
const sayDeveloper = () => 'developer'


let kalimat = `${saySaya()} ${sayAdalah()} ${saySeorang()} ${sayFrontend()} ${sayDeveloper()}`
console.log(kalimat)
console.log('\n')


// soal No 3

class Book {
    constructor(name, totalPage, price) {
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
    
}

class Komik extends Book {
    constructor(name, totalPage, price, isColorful) {
        super(name, totalPage, price)
        this.isColorful = isColorful
    }
}

komik1 = new Komik('One Piece',25, 15000, false)
console.log(komik1)



