import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <div className="container">
        <h1>Form Pembelian Buah</h1>
        <form>
          <div className="box">
            <label className="title-label" for="nama-pelanggan">Nama Pelanggan</label>
            <input type="text" id="nama-pelanggan"></input>
          </div>
          <div className="box">
            <label className="title-label" for="daftar-item">Daftar Item</label>
            <ul>
              <li><input type="checkbox" id="daftar-item" value="Semangka"></input><label>Semangka</label></li>
              <li><input type="checkbox" id="daftar-item" value="Semangka"></input><label>Jeruk</label></li>
              <li><input type="checkbox" id="daftar-item" value="Semangka"></input><label>Nanas</label></li>
              <li><input type="checkbox" id="daftar-item" value="Semangka"></input><label>Salak</label></li>
              <li><input type="checkbox" id="daftar-item" value="Semangka"></input><label>Anggur</label></li>
            </ul>
          </div>
          <button type="submit">Kirim</button>
        </form>
      </div>
    </div>
  );
}

export default App;
