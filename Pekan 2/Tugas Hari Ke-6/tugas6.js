// soal no 1

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var objekDaftarPeserta = {
    nama: 'Asep',
    "jenis kelamin": 'laki-laki',
    hobi: 'baca buku',
    'tahun lahir': 1992
}

console.log(objekDaftarPeserta)


// soal no 2

var buah = [{nama: 'strawberry', warna: 'merah', 'ada bijinya':'tidak', harga: 9000},
            {nama: 'jeruk', warna: 'oranye', 'ada bijinya': 'ada', harga: 8000},
            {nama: 'semangka', warna: 'hijau & merah','ada bijinya': 'ada', harga: 10000},
            {nama: 'pisang', warna: 'kuning', 'ada bijinya': 'tidak', harga: 5000}
            ]

console.log(buah[0])


// soal no 3

function tambahDataObjek(nama, durasi, genre, tahun) {
    obj = {
            nama: nama,
            durasi: durasi,
            genre: genre,
            tahun: tahun
        }
    return obj
}

var dataFilm = []
dataFilm.push(tambahDataObjek('Titanic', 210,'Drama', 1997))
dataFilm.push(tambahDataObjek('One Piece: Stampede', 101, 'Animasi', 2019))
console.log(dataFilm)
console.log('\n')


// soal no 4

class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cool_blooded = false
    }

    get namaAnimal() {
        return this.name
    }

    set namaAnimal(name) {
        this.nama = name
    }

    get coolBlooded() {
        return this.cool_blooded
    }

    set coolBlooded(coolblooded) {
        this.cool_blooded = coolblooded
    }


    get jumlahKaki() {
        return this.legs
    }

    set jumlahKaki(legs) {
        this.legs = legs
    }
}

class Ape extends Animal{
    constructor(name) {
        super(name)
    }

    yell() {
        console.log('Auooo')
    }

}

class Frog extends Animal{
    constructor(name) {
        super(name)
    }

    jump() {
        console.log('hop hop')
    }

}



var sheep = new Animal('shaun')
console.log(sheep.namaAnimal)
console.log(sheep.jumlahKaki)
console.log(sheep.coolBlooded)



var sungokong = new Ape('kera sakti')
sungokong.jumlahKaki = 2
console.log(sungokong.jumlahKaki)
sungokong.yell()


var kodok = new Frog('buduk') 
kodok.jump()

console.log(sheep)
console.log(sungokong)
console.log(kodok)



// soal no 5


class Clock {

    constructor({template}) {
        this.template = template   
    }
    
     render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start () {
      this.render()
      this.timer = setInterval(this.render.bind(this), 1000);
    }
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start(); 