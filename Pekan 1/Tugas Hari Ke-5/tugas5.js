

// soal No 1

function halo() {
    return 'Halo Sanbers!'
}

console.log(halo())


// soal No 2

function kalikan(number1,number2) {
    return number1 * number2
}

var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1,num2)
console.log(hasilKali)


// soal No 3

var introduce = function(name,age,address,hobby) {
    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`
}

var name = "Iqbal"
var age = 22
var address = 'jalan belum jadi'
var hobby = 'Gaming'

var perkenalan = introduce(name,age,address,hobby)
console.log(perkenalan)

