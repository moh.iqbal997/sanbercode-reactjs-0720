// Soal No 1

console.log('LOOPING PERTAMA')

var number = 0
var txt = 'I love coding'

while(number < 20) {
    number += 2
    console.log( number + ' - ' + txt)
}

console.log('LOOPING KEDUA')

txt = 'I will become a frontend developer'

while(number > 0) {
    console.log( number + ' - ' + txt)
    number -= 2
}


// soal No 2

console.log('\n\n\n\n')
for (var i = 1; i <= 20; i++) {
    if (i % 2 == 0) {
        console.log(i + ' - Berkualitas') 
    } else {
        if (i % 2 == 1 && i % 3 == 0) {
            console.log(i + ' - I Love Coding')
        } else {
            console.log(i + ' - Santai')
        }
    }
}


// soal no 3
console.log('\n\n\n')

var hastag = ''

for (var i = 0; i < 7; i++) {
    for (var j = 0; j < i; j++) {
        hastag += '#'
    }
    hastag += '\n'
}

console.log(hastag)


// soal no 4

console.log('\n\n\n')

var kalimat="saya sangat senang belajar javascript"

var kalimat_to_Array = kalimat.split(" ")
console.log(kalimat_to_Array)



// soal no 5

console.log('\n\n\n')

var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()

for (var i = 0; i < daftarBuah.length; i++) {
    console.log(daftarBuah[i])
}
